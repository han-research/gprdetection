# A GANs-based Deep Learning Framework for Automatic Subsurface Object Recognition from Ground Penetrating Radar Data


## Abstract

Ground penetrating radar (GPR) is a well-known useful tool for subsurface exploration. GPR data can be recorded at a relatively high speed in a continuous way with hyperbolas being artifacts and evidence of disturbances in the soil. Automatic and accurate detection and interpretation of hyperbolas in GPR remains an open challenge. Recently deep learning techniques have achieved remarkable success in image recognition tasks and this has potential for interpretation of GPR data. However, training reliable deep learning models requires massive labeled data, which is challenging. To address the challenges, this work proposes a Generative Adversarial Nets (GANs)-based deep learning framework, which generates new training data to address the scarcity of GPR data, automatically learns features and detects subsurface objects (via hyperbola) through an end-to-end solution. We have evaluated our proposed approach using real GPR B-scan images from rail infrastructure monitoring applications and compared this with the state-of-the-art methods for object detection (i.e. Faster-RCNN, Cascade R-CNN, SSD and YOLO V2). The proposed approach outperforms the existing methods with high accuracy of 97\% being the mean Average Precision (mAP). Moreover, the proposed approach also demonstrates the good generalizability through cross-validation on independent datasets.

## Getting started

GPRgen-generating.ipynb Using gprmax to generate simulation data

GPR-detection.ipynb Hyperbola recognition model training



## Authors and acknowledgment
This work is funded by innovate UK funded project (INFRAMONIT, grant ID: 104245).

## Citation
```
@article{zhang2021gans,
  title={A GANs-based deep learning framework for automatic subsurface object recognition from ground penetrating radar data},
  author={Zhang, Xin and Han, Liangxiu and Robinson, Mark and Gallagher, Anthony},
  journal={IEEE Access},
  volume={9},
  pages={39009--39018},
  year={2021},
  publisher={IEEE}
}
```

